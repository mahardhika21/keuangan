<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    use HasFactory;

    protected $table = 'administrator';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'full_name', 'address', 'web', 'photo', 'gender'
    ];

    public static function laratablesCustomAction($admin)
    {
        return view('backoffice.admin.action', compact('admin'))->render();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
