<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nama', 'type', 'created_at', 'updated_at'
    ];

    public static function laratablesCustomAction($kategori)
    {
        return view('backoffice.kategori.action', compact('kategori'))->render();
    }

    public function transaksi()
    {
        return $this->belongsTo(Member::class, 'member_id');
    }
}
