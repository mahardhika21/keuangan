<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksi';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'kategori_id', 'nominal', 'deskripsi', 'tanggal',  'created_at'
    ];

    public static function laratablesCustomAction($transaksi)
    {
        return view('backoffice.transaksi.action', compact('transaksi'))->render();
    }

    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id');
    }

    public function villages()
    {
        return $this->belongsTo(Villages::class, 'villages_id');
    }
}
