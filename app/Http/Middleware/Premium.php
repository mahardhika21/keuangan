<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Premium
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(Auth::user()->active == 'false') {
            return response()->json([
                "success" => "false",
                "message" => "anda belum dapat menggunakan menu ini, silakan melakukan pemabayaran tabungan"
            ], 403);
        }
        return $next($request);
    }
}
