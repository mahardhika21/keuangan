<?php
namespace App\Http\Services;

class InformationLinusa
{
    public function trx($type)
    {
            if($type == 'wd') {
                return 'Withdraw (penarikan saldo member)';
            }elseif($type == 'tf') {
                return 'Top-up Saldo Member';
            }elseif($type == 'wajib') {
                return 'Pembayaran Iuran Wajib';
            }elseif($type == 'pokok') {
                return 'Pembayaran Iuran Pokok';
            }elseif($type == 'dompet') {
                return 'dompet';
            }else{
                return '-';
            }
    }

    public function status($no)
    {
        if($no == '1') {
            return "Transaksi Berhasil";
        }elseif($no == '2') {
            return "Transaksi Dalam Proses";
        }elseif($no == '0') {
            return "Transaksi Gagal";
        }
    }
}
