<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Freshbitsweb\Laratables\Laratables;
use App\Models\Administrator;
use App\Models\User;
use App\Http\Requests\Backoffice\AdminRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Services\UploadServices;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.admin.index', [
            "title"   => "list : admin",
            "menu"    => "admin",
        ]);
    }

    public function datatables(Request $request)
    {
        return Laratables::recordsOf(Administrator::class, function ($query) {
            return $query->select('administrator.photo', 'administrator.id as administrator_id', 'users.role', 'users.email', 'users.name', 'users.active')
                ->join('users', 'users.id', '=', 'administrator.user_id')
                ->where('users.role', '!=', 'superadmin');
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.admin.form', [
            "title"  => "Tambah admin",
            "action" => "create",
            "menu"   => "admin",
            "admin" => new Administrator(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
        // echo $request->role; die();
        // die();
        DB::beginTransaction();
        try {
            $user = new User();
            $user->fill($request->all());
            $user->password = bcrypt($request->password);
            $user->role = $request->role;
            $user->save();
            $admin = new Administrator();
            $admin->user_id = $user->id;
            $admin->save();
            DB::commit();
            return redirect()->route('backoffice.admin.index')->with('message_success', 'success register new admin');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('backoffice.admin.index')->with('error_success', 'failed register new admin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return redirect()->route('backoffice.admin.index')->with('message_success', 'success delete  admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Administrator $admin)
    {
        $user = User::find($admin->user_id);
        $user->active = $user->active == 1 ? '0' : '1';
        $user->save();

        return redirect()->route('backoffice.admin.index')->with('message_success', 'success change status admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return redirect()->route('backoffice.admin.index')->with('message_success', 'success delete  admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Administrator $admin)
    {
        DB::beginTransaction();
        try {
            Administrator::where('id', $admin->id)->delete();
            User::where('id', $admin->user_id)->delete();
            DB::commit();
            return redirect()->route('backoffice.admin.index')->with('message_success', 'success delete  admin');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('backoffice.admin.index')->with('message_error', 'success delete  admin' . $e->getMessage());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImg(Request $request)
    {
        $file  = new UploadServices();
        $img = $file->uploadSingleFile($request->foto, 'img/summernote');

        return response()->json(['sucees' => 'true', 'message' => 'success upload data', 'url' => $img]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteImg(Request $request)
    {
        if (File::exists($request->url)) {
            File::delete($request->url);
        }

        return response()->json(['sucees' => 'true', 'message' => 'success delet data']);
    }
}
