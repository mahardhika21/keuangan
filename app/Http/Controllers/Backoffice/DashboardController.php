<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (!empty($request->starDate) and !empty($request->endDate)) {
            $filter = "filter=".$request->starDate.'|'.$request->endDate;
        } else {
            $filter = '';
        }
        return view('backoffice.dashboard.index', [
            'title' => 'Dashboard Transasksi',
            'menu'  => 'home',
            'filter' => $filter,
            'star'   => $request->starDate,
            'end'    => $request->endDate,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function dashboardInfo()
    {
        $pendapatan = DB::table('transaksi')->select('transaksi.nominal')
                    ->join('kategori', 'kategori.id', '=', 'transaksi.kategori_id')
                    ->where('kategori.type', 'pemasukkan')->sum('transaksi.nominal');
        $pengeluaran = DB::table('transaksi')->select('transaksi.nominal')
                    ->join('kategori', 'kategori.id', '=', 'transaksi.kategori_id')
                    ->where('kategori.type', 'pengeluaran')->sum('transaksi.nominal');
         $saldo = $pendapatan - $pengeluaran;
        $data = [
            "pendapatan" => $pendapatan,
            "pengeluaran" => $pengeluaran,
            "saldo" => $saldo,
        ];
        return response()->json(['success' => 'true', 'message' => 'success get data', 'data' => $data], 200);
    }
}
