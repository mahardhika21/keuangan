<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Models\User;
use App\Mail\ResetPasswordAdmin;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            return redirect('backoffice/dashboard');
        }
        return view('backoffice.auth.login', [
            'title' => 'Login Page',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function forget()
    {
        if (Auth::check()) {
            return redirect('backoffice/dashboard');
        }
        return view('backoffice.auth.forget', [
            'title' => 'Lupa Password',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        return view('backoffice.auth.reset', [
            'title' => 'Reset Password',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $request->validate([
            "username" => 'required|string',
            'password' => 'required|string|min:5',
        ]);

        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $credentials = [
                "email"    => $request->username,
                "password" => $request->password,
                "active"   => 1,
            ];
        } else {
            $credentials = [
                "username" => $request->username,
                "password" => $request->password,
                "active"   => 1,
            ];
        }

        if (Auth::attempt($credentials)) {
            return redirect('backoffice/dashboard');
        } else {
            return redirect()->back()->with('error_message', 'login failed!, username/email, password dont exits or user not active');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        return redirect()->route('backoffice.auth.login')->with('success_message', 'logout success !!');
    }



    public function submitForget(Request $request)
    {
        $request->validate([
            "email" => 'required|email'
        ]);

        $user = User::where('email', $request->email)->first();

        if (empty($user)) {
            return redirect()->back()->with('error_message', 'email admin/user not found');
        }

        try {
            $token   = $this->generateCode($user->id, $request->email);
            $link    = url('/') . '/backoffice/reset/' . $token;
            $arrdata = array('nama' => $user->name, 'code' => $link,);

            Mail::to($request->email)->send(new ResetPasswordAdmin($arrdata));

            return redirect()->back()->with('success_message', 'Success, cek email untuk reset passowod');
        } catch (\Excetption $e) {

            return redirect()->back()->with('error_message', $e->getMessage());
        }
    }

    private function generateCode($user_id, $email)
    {
        $code = Str::random(10);
        $token = Crypt::encryptString(strtotime(date('Y-m-d H:i:s')) + (30 * 60) . '-' . $code . '-' . $email);
        User::where('id', $user_id)->update(['password_token' => $token]);

        return $token;
    }

    public function resetPassword(ForgotRequest $request)
    {
        $key = $request['key'];
        $data = User::where('password_token', $key)->first();

        if (!empty($data)) {
            DB::beginTransaction();
            try {
                User::where('id', $data->id)->update(['password' => bcrypt($request->password), 'password_token' => null]);

                DB::commit();
                return redirect('backoffice.auth.index')->with('success_message', 'Success reset password');
            } catch (\Illuminate\Database\QueryException $e) {

                return redirect()->back()->with('error_message', $e->getMessage());
            }
        } else {
            return redirect()->back()->with('error_message', 'Failed, reset password key sudah kadaluarsa');
        }
    }
}
