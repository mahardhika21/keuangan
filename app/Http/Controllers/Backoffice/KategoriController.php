<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backoffice\KategoriRequest;
use App\Models\Kategori;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Auth;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backoffice.kategori.index', [
            "title" => 'Kategori Transaksi',
            "menu"  => 'kategori',
        ]);
    }

    /**
     * get data tables from resource resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function datatables(Request $request)
    {
        return Laratables::recordsOf(Kategori::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.kategori.form', [
            "title"   => "tambah kategori",
            "menu"    => "kategori",
            "action"  => "create",
            "kategori" => new Kategori(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KategoriRequest $request)
    {
        $kategori = new Kategori();
        $kategori->fill($request->all());
        $kategori->save();

        return redirect()->route('backoffice.kategori.index')->with('message_success', 'success inser data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('backoffice.Kategori.form', [
            "title"   => "update kategori",
            "menu"    => "Kategori",
            "action"  => "edit",
            "kategori" => $kategori,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KategoriRequest $request, $id)
    {
        $kategori = Kategori::find($id);
        $kategori->fill($request->all())->update();
        return redirect()->route('backoffice.kategori.index')->with('message_success', 'success update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        $kategori->delete();
        return redirect()->route('backoffice.kategori.index')->with('message_success', 'success delete data');
    }

    public function kategoryType($type)
    {

        //echo "dasdasda";
        return Kategori::select('id', 'nama')->where('type', $type)->get();
    }
}
