<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backoffice\TransaksiRequest;
use App\Models\Transaksi;
use Freshbitsweb\Laratables\Laratables;
use Illuminate\Support\Facades\Auth;
use App\Models\Kategori;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         if (!empty($request->starDate) and !empty($request->endDate)) {
            $filter = "filter=".$request->starDate.'|'.$request->endDate;
        } else {
            $filter = '';
        }
        return view('backoffice.dashboard.index', [
            "title" => 'Transaksi',
            "menu"  => 'kategori',
            'filter' => $filter,
            'star'   => $request->starDate,
            'end'    => $request->endDate,
        ]);
    }

    /**
     * get data tables from resource resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function datatables(Request $request)
    {
        return Laratables::recordsOf(Transaksi::class, function ($query) use ($request) {
               if(!empty($request->filter)) {
                    $filter = explode('|', $request->filter);
                    return $query->wheredate('tanggal', [$filter[0], $filter[1]]);
                }else {
                     return $query;
                }
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.transaksi.form', [
            "title"   => "Tambah transaksi",
            "menu"    => "transaksi",
            "action"  => "create",
            "transaksi" => new Transaksi(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransaksiRequest $request)
    {
        $transaksi = new Transaksi();
        $transaksi->fill($request->all());
        $transaksi->user_id = Auth::user()->id;
        $transaksi->save();

        return redirect()->route('backoffice.transaksi.index')->with('message_success', 'success insert data');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        return view('backoffice.transaksi.form', [
            "title"   => "update transaksi",
            "menu"    => "transaksi",
            "action"  => "edit",
            "transaksi" => $transaksi,
            "kategori" => Kategori::where('type', $transaksi->kategori->type)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransaksiRequest $request, $id)
    {
        $transaksi = Transaksi::find($id);
        $transaksi->fill($request->all())->update();
        return redirect()->route('backoffice.transaksi.index')->with('message_success', 'success update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        $transaksi->delete();
        return redirect()->route('backoffice.transaksi.index')->with('message_success', 'success delete data');
    }
}
