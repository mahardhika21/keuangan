<?php

namespace App\Http\Requests\Backoffice;

use Illuminate\Foundation\Http\FormRequest;

class KategoriRequest extends FormRequest
{
    /* Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->action == 'create') {
            return [
                "nama"        => "required",
                "type"      => "required"
            ];
        } else {
            return [
                "nama"        => "required|min:3|unique:kategori,nama," . $this->Kategori_id,
                "type"      => "required"
            ];
        }
    }
}
