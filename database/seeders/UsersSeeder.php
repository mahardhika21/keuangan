<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Kategori;
use App\Models\Transaksi;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'     => 'Zulkifli Mahardhika',
            'email'    => 'user@user.id',
            'password' => bcrypt('123456'),
            'role'     => 'user',
        ]);

        $kategori = [
            [
                "nama" => "gaji",
                "type" => "pemasukkan",
            ],
            [
                "nama" => "tunjangan",
                "type" => "pemasukkan",
            ],
            [
                "nama" => "bonus",
                "type" => "pemasukkan",
            ],
            [
                "nama" => "sewa kost",
                "type" => "pengeluaran",
            ],
            [
                "nama" => "makan",
                "type" => "pengeluaran",
            ],
            [
                "nama" => "pakaian",
                "type" => "pengeluaran",
            ]
        ];

        foreach($kategori as $data) {
            Kategori::create([
                'nama'     => $data['nama'],
                'type'    =>  $data['type']
            ]);
        }
    }
}
