<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backoffice\DashboardController;
use App\Http\Controllers\Backoffice\AuthController;
use App\Http\Controllers\Backoffice\TransaksiController;
use App\Http\Controllers\Backoffice\KategoriController;

Route::get('login', [AuthController::class, 'index'])->name('backoffice.auth.login');
Route::post('login', [AuthController::class, 'login'])->name('backoffice.atuh.login');
Route::get('forget', [AuthController::class, 'forget'])->name('backoffice.auth.forget');


Route::group(['middleware' => ['role:all'], 'as' => 'backoffice.'], function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('info_bar', [DashboardController::class, 'dashboardInfo']);
    Route::get('logout', [AuthController::class, 'logout'])->name('auth.logout');
    Route::get('kategori/datatables', [KategoriController::class, 'datatables'])->name('kategori.datatables');
    Route::get('kategori/type/{type}', [KategoriController::class, 'kategoryType'])->name('kategori.type');
    Route::resource('kategori', KategoriController::class);
    Route::get('transaksi/datatables', [TransaksiController::class, 'datatables'])->name('transaksi.datatables');
    Route::resource('transaksi', TransaksiController::class);
});
