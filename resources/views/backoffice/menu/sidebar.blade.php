<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background-color:#6833FF;">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link" style="background-color:#6833FF;">
        <img src="https://adminlte.io/themes/v3/dist/img/AdminLTELogo.png" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">LinusaAdmin</span>
    </a>
    <!-- Sidebar -->
    <div
        class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
        <div class="os-resize-observer-host observed">
            <div class="os-resize-observer" style="left: 0px; right: auto;"></div>
        </div>
        <div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;">
            <div class="os-resize-observer"></div>
        </div>
        <div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 100px;"></div>
        <div class="os-padding">
            <div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;">
                <div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="https://adminlte.io/themes/v3/dist/img/user2-160x160.jpg"
                                class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">ADMIN</a>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="{{ url('/') }}" class="nav-link {{ $menu == 'home' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-home"></i>
                                    <p>
                                        Dashboard
                                        {{-- <i class="right fas fa-angle-left"></i> --}}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-header">PENGATURAN</li>
                            <li class="nav-item">
                                <a href="{{ route('backoffice.admin.index') }}"
                                    class="nav-link {{ $menu == 'admin' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Administator
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item {{ $menu == 'profile' ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link {{ $menu == 'profile' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Profile
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.profile.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Data Profile</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.profile.password') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Update Password</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <li class="nav-header">MEMBER</li>
                           <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Member
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('backoffice.member.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Member Aktif</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('backoffice.member.index') }}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Member Register</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                         <li class="nav-header">DATA</li>
                            <li class="nav-item {{ $menu == 'master' ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-money-bill"></i>
                                    <p>
                                       Simpanan
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.iuran.show', 'wajib') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Simpanan Wajib</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.iuran.show', 'pokok') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Simpanan Pokok</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('backoffice.program.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-file-signature"></i>
                                    <p>Program</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('backoffice.santunan.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-receipt"></i>
                                    <p>Santunan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('backoffice.withdraw.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-money-check-alt"></i>
                                    <p>Withdraw Penarikan</p>
                                </a>
                            </li>
                            <li class="nav-item {{ $menu == 'news' ? 'menu-open' : '' }}">
                                <a href="{{ route('backoffice.news.index') }}" class="nav-link {{ $menu == 'profile' ? 'active' : '' }}">
                                    <i class="nav-icon fas fa-newspaper"></i>
                                    <p>
                                        Artikel
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.news.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>List Artikel</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.news_cat.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Kategori Berita</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item {{ $menu == 'info' ? 'menu-open' : '' }}">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-info"></i>
                                    <p>
                                        Info
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.bank.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Bank</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.kategori.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Kategori</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.faq.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>FAQ</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.info.show','about') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>About</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.info.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Contact</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-header">TRANSAKSI</li>
                            <li class="nav-item {{ $menu == 'transaksi' ? 'menu-open' : '' }}">
                                <a href="{{ route('backoffice.transaksi.index') }}" class="nav-link">
                                    <i class="nav-icon fas fa-money-bill"></i>
                                    <p>
                                        Transaksi
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.transfer.index') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Transfer Member</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.transaksi.index', 'type=pokok') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Pembayaran Iuran Pokok</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.transaksi.index', 'wajib') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Pembayaran Iuran Wajib</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('backoffice.iuran.show', 'pokok') }}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Pembayaran Program</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-header">Laporan</li>
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="nav-icon fas fa-file"></i>
                                    <p>Laporan</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div>
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="height: 11.7124%; transform: translate(0px, 0px);"></div>
            </div>
        </div>
        <div class="os-scrollbar-corner"></div>
    </div>
    <!-- /.sidebar -->
</aside>
