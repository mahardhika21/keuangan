<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }}<a href="#"> XDZ</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        {{-- <b>Version</b> 3.1.0-pre --}}
    </div>
</footer>
</div>

<script>
    window.onload = () => {
        let http = new XMLHttpRequest();
        http.onreadystatechange = function () {
            if(this.readyState == 4 && this.status == 200) {
                let jdata = JSON.parse(this.responseText);
                document.getElementById('saldo').innerHTML = jdata.data.saldo;
                document.getElementById('pendapatan').innerHTML = jdata.data.pendapatan;
                document.getElementById('pengeluaran').innerHTML = jdata.data.pengeluaran;
            }
        }

        http.open('GET', window.location.origin +'/backoffice/info_bar', true);
        http.send();
    }

</script>
