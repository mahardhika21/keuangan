@extends('backoffice.layouts.app-top')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8 offset-md-2">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ $action }} </h3>
                        </div>
                        @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <div style="margin-bottom:20px; font-size:13px; font-style:italic">
                                Semua inputan harus diisi.
                            </div>
                            @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                            @endif
                            <form method="POST"
                                action="{{ $action == 'create' ? route('backoffice.transaksi.store') : route('backoffice.transaksi.update', $transaksi->id) }}"
                                enctype="multipart/form-data">
                                @csrf
                                @if($action == 'edit')
                                @method('PUT')
                                <input type="hidden" name="kategori_id" value="{{ $transaksi->id }}">
                                @endif
                                <div class="form-row">
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Nominal</label>
                                        <input type="hidden" value="{{ $action }}" name="action">
                                        <input type="number" class="form-control @error('name') is-invalid @enderror"
                                            placeholder="nominal" name="nominal" value="{{ old('nama', $transaksi->nominal) }}">
                                        @error('nominal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Tanggal Transaksi</label>
                                        <input type="hidden" value="{{ $action }}" name="action">
                                        <input type="date" class="form-control @error('tanggal') is-invalid @enderror" placeholder="tanggal" name="tanggal"
                                            value="{{ old('nama', $transaksi->tanggal) }}">
                                        @error('tanggal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Tipe</label>
                                        <select id="type" class="form-control @error('type') is-invalid @enderror" name="type">
                                            @if($action == 'create')
                                            <option value="pemasukkan">
                                                pilih data
                                            </option>
                                            <option value="pemasukkan">
                                                pemasukkan
                                            </option>
                                            <option value="pengeluaran">
                                                Pengeluaran
                                            </option>
                                            @else
                                            <option value="pemasukkan" {{ (old('type', $transaksi->kategori->type) == 'pemasukkan') ? 'selected' : '' }}>
                                                pemasukan
                                            </option>
                                            <option value="pengeluaran" {{ (old('type', $transaksi->kategori->type) == 'pengeluaran') ? 'selected' : '' }}>
                                                Pengeluaran
                                            </option>
                                            @endif
                                        </select>
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Kategori</label>
                                        <select id="kategori" required class="form-control @error('kategori_id') is-invalid @enderror" name="kategori_id">
                                            @if($action == 'edit')
                                            @foreach($kategori as $dt)
                                            <option value="{{ $dt->id }}" {{ $transaksi->kategori_id == $dt->id ? 'selected' : '' }}>{{ $dt->nama }}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        @error('kategori')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Keterangan</label>
                                        <textarea type="text" class="form-control @error('deskripsi') is-invalid @enderror" placeholder="deskripsi"
                                            name="deskripsi" value="{{ old('deskripsi', $transaksi->deskripsi) }}"></textarea>
                                        @error('deskripsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary"
                                            name="button_submit">Simpan</button>
                                        <a onclick="windows.back.history()" style="color: azure;cursor:pointer;"
                                            class="btn btn-danger">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
    $('#type').on('change', function(e) {
        let type = e.target.value;

        $.get('{{ url("/") }}/backoffice/kategori/type/' + type, function(data) {
            $('#kategori').empty();
          //  alert(data);
            $('#kategori').append(`<option value="-">Pilih kateogiri</option>`);
            $.each(data, function(index, subCatObj) {
              // console.log(index);
                $('#kategori').append(`<option value="${subCatObj.id}">${subCatObj.nama}</option>`);

            });
        });
    });
</script>
@endsection
