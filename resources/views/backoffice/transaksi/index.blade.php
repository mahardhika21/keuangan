@extends('backoffice.layouts.app-top')
@section('css')
<link href="{{ asset('storage/assets/backoffice/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Member</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"> Member</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box">
                                <span class="info-box-icon bg-info elevation-1"><i class="fas fa-money-bill-wave"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Tabungan Pokok</span>
                                    <span class="info-box-number" id="tabungan_pokok">

                                    </span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-money-bill-wave"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Tabungan Wajib</span>
                                    <span class="info-box-number" id="tabungan_wajib"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <!-- fix for small devices only -->
                        <div class="clearfix hidden-md-up"></div>
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-file"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Program</span>
                                    <span class="info-box-number" id="program"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-12 col-sm-6 col-md-3">
                            <div class="info-box mb-3">
                                <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text"> Members</span>
                                    <span class="info-box-number" id="member"></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>
                        </div>
                        <div class="form-group" style="margin-top: 10px">
                            <div class="col-sm-10">
                                <a href="{{ route('backoffice.member.create') }}" class="btn btn-primary">Tambah
                                    Data Nasabah</a>
                            </div>
                        </div>
                        <div class="form-group" style="margin-top: 10px">
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                            @endif
                            @if (session('message_error'))
                            <div class="alert alert-error">
                                {{ session('message_error') }}
                            </div>
                            @endif
                            <table id="dataTable" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Saldo</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
<!-- /.content -->
@endsection
@section('js')
<script src="{{ asset('storage/assets/backoffice/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('storage/assets/backoffice/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    //function OnEditing button
    function OnEditing() {
        $("table#dataTable > tbody > tr > td > form").delegate('a.edit-data', 'click', function() {
        var elemt = $(this).closest('table#dataTable');
        var pages = elemt.DataTable().page.info().page
        localStorage.setItem('my_page', pages);

        return true;
        });
    }
    $('#dataTable').DataTable({
        serverSide: true,
        processing: true,
        responsive: true,
        ajax: "{{ route('backoffice.member.datatables') }}",
        "drawCallback": function(settings) {
        //create sesi on button
        OnEditing()
        },
        columns: [
            { name: 'member_id', searchable: false },
            { name: 'nama', },
            { name: 'saldo' },
            { name: 'action', orderable: false, searchable: false }
        ],
        columnDefs : [
         {
            targets: 0,
            orderable: true,
            render: function ( data, type, row, meta ){
            return meta.row + meta.settings._iDisplayStart + 1;
         }
         }
        ],
    });

    if(localStorage.getItem('my_page') != '') {
        var elemt = $('table#dataTable').DataTable();
        elemt.ajax.reload(function() {
        var pages = parseInt(localStorage.getItem('my_page'));
        setTimeout(function() {
        elemt.page((!pages?0:pages)).draw('page')
        setTimeout(function() {
        //hapus sesi
        localStorage.removeItem('my_page')
        }, 3000)
        }, 0) // default 0
        });

      //  return true;
    }
</script>

@endsection
