<form action="{{ route('backoffice.transaksi.destroy', $transaksi->id) }}" method="post">
    @csrf
    @method('DELETE')
    <a href="{{ route('backoffice.transaksi.edit', $transaksi->id) }}" class="btn btn-secondary btn-sm edit-data">
        Edit
    </a>
    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Hapus data ini ?')">Hapus</button>
</form>
