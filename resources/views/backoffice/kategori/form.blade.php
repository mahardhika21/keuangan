@extends('backoffice.layouts.app-top')
@section('css')

@endsection
@section('content')
<div class="content-wrapper" style="min-height: 180px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ $title }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Admin</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-8 offset-md-2">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">{{ $action }} </h3>
                        </div>
                        {{-- @if($errors->any())
                        {{ implode('', $errors->all('<div>:message</div>')) }}
                        @endif --}}
                        <!-- /.card-header -->
                        <!-- form start -->
                        <div class="card-body">
                            <div style="margin-bottom:20px; font-size:13px; font-style:italic">
                                Semua inputan harus diisi.
                            </div>
                            @if (session('message_success'))
                            <div class="alert alert-success">
                                {{ session('message_success') }}
                            </div>
                            @endif
                            <form method="POST"
                                action="{{ $action == 'create' ? route('backoffice.kategori.store') : route('backoffice.kategori.update', $kategori->id) }}"
                                enctype="multipart/form-data">
                                @csrf
                                @if($action == 'edit')
                                @method('PUT')
                                <input type="hidden" name="kategori_id" value="{{ $kategori->id }}">
                                @endif
                                <div class="form-row">
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Nama</label>
                                        <input type="hidden" value="{{ $action }}" name="action">
                                        <input type="text" class="form-control @error('name') is-invalid @enderror"
                                            placeholder="Nama" name="nama" value="{{ old('nama', $kategori->nama) }}">
                                        @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label for="inputNama">Tipe</label>
                                        <select class="form-control @error('type') is-invalid @enderror" name="type" id="type">
                                            <option value="pemasukkan" {{ (old('jenkel', $kategori->type) == 'pemasukkan') ? 'selected' : '' }}>
                                                pemasukan
                                            </option>
                                            <option value="pengeluaran" {{ (old('jenkel', $kategori->type) == 'pengeluaran') ? 'selected' : '' }}>
                                                Pengeluaran
                                            </option>
                                        </select>
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary"
                                            name="button_submit">Simpan</button>
                                      <a onclick="windows.back.history()" style="color: azure;cursor:pointer;" class="btn btn-danger">Batal</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')

@endsection
